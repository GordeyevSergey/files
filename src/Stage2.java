import java.io.*;

/**
 * Класс, проверяющий целые числа на возможность представления их в шестизначном виде.
 */
public class Stage2 {
    /**
     * Метод проверяет считываемые из файла intdata.dat числа на соответствие диапазону значений
     * и записывает результат в файл int6data.dat.
     */
    public static void main(String[] args) throws IOException {
        File int6data = new File("src\\int6data.dat");
        DataOutputStream dataOutputStream = new DataOutputStream(new FileOutputStream(int6data));
        DataInputStream dataReader = new DataInputStream(new FileInputStream("src\\intdata.dat"));
        int number;
        while (dataReader.available() > 0) {
            number = dataReader.readInt();
            if (number == 0) {
                dataOutputStream.writeInt(number);
            } else if (number > 999 && number < 1000000) {
                dataOutputStream.writeInt(number);
            }
        }

        dataReader.close();
        dataOutputStream.close();
    }
}