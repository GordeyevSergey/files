import java.io.*;

/**
 * Класс для определения и записи "счастливых" чисел.
 */
public class Stage3 {
    /**
     * Метод считывает из файла int6.dat и записывает в файл txt6data.dat
     */
    public static void main(String[] args) throws IOException {
        File txt6data = new File("src\\txt6data.dat");
        DataInputStream dataReader = new DataInputStream(new FileInputStream("src\\int6data.dat"));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(txt6data));
        int number;
        while (dataReader.available() > 0) {
            number = dataReader.readInt();
            if (isLuckyNumber(number)) {
                bufferedWriter.write(number + "\n");
            }
        }

        dataReader.close();
        bufferedWriter.close();
    }

    /**
     * Метод проверяет число на "Счастливое"
     *
     * @param number проверяемое число
     * @return результат проверки
     */
    private static boolean isLuckyNumber(int number) {
        return (number % 10)+(number / 10 % 10)+(number / 100 % 10) == (number / 1000 % 10)+(number / 10000 % 10)+(number / 100000);
    }

}