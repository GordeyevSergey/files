import java.io.*;

/**
 * Класс для поиска целых чисел среди считываемых строк.
 */
public class Stage1 {
    /**
     * Метод считывает строки из файла input.java,
     * проверяет, является ли полученная строка целым числом,
     * записывает целые числа в файл intdata.dat
     */
    public static void main(String[] args) throws IOException {
        File intdata = new File("src\\intdata.dat");
        File error = new File("src\\error.txt");
        try{
        BufferedReader bufferedReader = new BufferedReader(new FileReader("src\\input.txt"));
        DataOutputStream dataWriter = new DataOutputStream(new FileOutputStream(intdata));
        BufferedWriter errorWriter = new BufferedWriter(new FileWriter(error));
        String string;
        int number;
        while ((string = bufferedReader.readLine()) != null) {
            try {
                number = Integer.parseInt(string);
                dataWriter.writeInt(number);
            } catch (NumberFormatException e) {
                errorWriter.write(e + "\n");
            }

        }

        bufferedReader.close();
        dataWriter.close();
        errorWriter.close();
        } catch (FileNotFoundException f) {
            Reserve.main();
        }
    }
}
