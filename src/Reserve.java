import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class Reserve {
    public static void main() throws IOException {
        Scanner in = new Scanner(System.in);
        File input = new File("src\\input.txt");
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(input));
        String string;
        System.out.println("Введите число. \n" + "Напишите end, если ввод закончен");
        do{
            string = in.nextLine();
            bufferedWriter.write(string + "\n");
        }while(string.equals("end") != true);
        bufferedWriter.close();
    }
}
